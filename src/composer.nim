import types, options, tables, algorithm

var forwardedNets: seq[int]

#TODO general optimization (use some graph library) (parallelisation)

proc getPin(c: Circuit, p: CircuitPin): Pin =
    c.components[p[0].r][p[0].n].pins[p[1]].pin

proc setPin(c: var Circuit, p: CircuitPin, val: Pin) =
    c.components[p[0].r][p[0].n].pins[p[1]] = (pin: val, conns: c.components[p[0].r][p[0].n].pins[p[1]].conns)

proc getPinConns(c: Circuit, p: CircuitPin): seq[uint] =
    return c.components[p.c.r][p.c.n].pins[p.p].conns

proc getNetFromPin(c: Circuit, p: CircuitPin): tuple[err: Option[CircuitError], n: Option[int]] =
    template search(s: seq[CircuitPin]): bool =
        var found = false
        for i in s:
            if i == p:
                found = true
                break
        found

    if c.ignore.search():
        return (err: none(CircuitError), n: none(int))
    else:
        for i in 0..c.nets.len() - 1:
            if c.nets[i].pins.binarySearch(p) != -1:
                return (err: none(CircuitError), n: some(i))
            
        return (err: some(CircuitError(msg: "The pin in unconnected", kind: UnconnectedPin, pin: p, net: none(Net), bus: none(Bus))), n: none(int))

proc callLogic(c: var Circuit, net: var Net, p: CircuitPin) =
    for conn in c.getPinConns(p):
        var p1 = (n: p.p, p: c.getPin(p))
        var p2 = (n: conn, p: c.getPin((c: p.c, p: conn)))
        c.components[p.c.r][p.c.n].logics[p.p](net, p1, p2)
        c.setPin(p, p1.p)
        c.setPin((c: p.c, p: conn), p2.p)

proc mergeBusesIntoNets(circuit: var Circuit) =
    for bus in circuit.buses:
        if bus.pinsToPin.isSome:
            var pinNet = circuit.getNetFromPin(bus.pinsToPin.get.pin)
            if pinNet.n.isSome:
                circuit.nets[pinNet.n.get].pins.add bus.pinsToPin.get.pin
                #circuit.nets[pinNet.n.get].pins.add bus.pinsToPin.get.pins

                for pin in bus.pinsToPin.get.pins:
                    var pinNet2 = circuit.getNetFromPin(pin)
                    if pinNet.n.isSome:
                        circuit.nets[pinNet.n.get].pins.add circuit.nets[pinNet2.n.get].pins
                        circuit.nets.delete pinNet2.n.get
                    else:
                        circuit.nets[pinNet.n.get].pins.add pin
            else:
                var newNet = Net(pins: @[bus.pinsToPin.get.pin])
                for pin in bus.pinsToPin.get.pins:
                    var pinNet = circuit.getNetFromPin(pin)
                    if pinNet.n.isSome:
                        newNet.pins.add circuit.nets[pinNet.n.get].pins
                        circuit.nets.delete pinNet.n.get
                    else:
                        newNet.pins.add pin
                circuit.nets.add newNet
        else:
            for pinkey in bus.pinsToPins.get.keys:
                var keyNet = circuit.getNetFromPin(pinkey)
                var valueNet = circuit.getNetFromPin(bus.pinsToPins.get[pinkey])
                
                if keyNet.n.isSome and valueNet.n.isSome:
                    circuit.nets[keyNet.n.get].pins.add circuit.nets[valueNet.n.get].pins
                    circuit.nets.delete valueNet.n.get
                elif not keyNet.n.isSome and not valueNet.n.isSome:
                    circuit.nets.add Net(pins: @[pinkey, bus.pinsToPins.get[pinkey]])
                else:
                    if keyNet.n.isSome:
                        circuit.nets[keyNet.n.get].pins.add bus.pinsToPins.get[pinkey]
                    else:
                        circuit.nets[valueNet.n.get].pins.add pinkey

proc fowardNet(circuit: var Circuit, net: int): seq[CircuitError] =
    var forward: seq[CircuitPin]

    var check_late: seq[Pin]
    var current = (0.0, 0.0)
    var voltage = (0.0, 0.0)
    for pin in circuit.nets[net].pins:
        case circuit.getPin(pin).kind:
            of Input:
                circuit.callLogic(circuit.nets[net], pin)

                current = (circuit.getPin(pin).minCurrent + current[0], circuit.getPin(pin).maxCurrent + current[1])
                if circuit.getPin(pin).minVoltage > voltage[0]:
                    voltage[0] = circuit.getPin(pin).minVoltage
                if circuit.getPin(pin).maxVoltage < voltage[1] or voltage[1] == 0:
                    voltage[1] = circuit.getPin(pin).maxVoltage
                if voltage[0] > voltage[1]:
                    result.add CircuitError(msg: "The net requieres an unsuitable Voltage for the pins connected in it", kind: IncorrectVoltage, net: some(circuit.nets[net]), bus: none(Bus))
                    voltage[1] = voltage[0]

                for p in circuit.getPinConns(pin):
                    forward.add (c: pin.c, p: p)
                    if circuit.getPin((c: pin.c, p: p)).kind == Output:
                        var p1 = circuit.getPin((c: pin.c, p: p))
                        p1.needforward = false
                        circuit.setPin((c: pin.c, p: p), p1)
            of Output:
                if not circuit.getPin(pin).needforward or net == 0:
                    check_late.add circuit.getPin(pin)
                else:
                    return result
            else:
                #Does nothing??
                #TODO
                discard
    
    for pin in check_late:
        if current[0] >= 0:
            current[0] = current[0] - pin.minCurrent

        if current[1] >= pin.maxCurrent:
            current[1] = current[1] - pin.maxCurrent
        else:
            result.add CircuitError(msg: "The net has an unsuitable Amperage for the pins connected in it", kind: IncorrectAmperage, net: some(circuit.nets[net]), bus: none(Bus))

        #TODO: Voltage

    circuit.nets[net] = circuit.nets[net].private_assign_net_data(voltage[0], voltage[1], current[0], current[1])
    forwardedNets.add net

    for pin in forward:
        var p = circuit.getNetFromPin(pin)
        if p.n.isSome:
            for net in forwardedNets:
                if p.n.get == net:
                    return result
            result.add circuit.fowardNet(p.n.get)
        else:
            if p.err.isSome:
                result.add p.err.get


proc compose*(circuit: Circuit, inNets: uint): tuple[circuit: Circuit, errors: seq[CircuitError]] =
    forwardedNets = @[]
    var r = circuit
    var errs: seq[CircuitError]
    mergeBusesIntoNets(r)
    for i in 0..inNets-2:
        if r.nets[i].isNetAssigned:
            errs.add fowardNet(r, i.int)
    return (circuit: r, errors: errs)

